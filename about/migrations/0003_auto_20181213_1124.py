# Generated by Django 2.1.1 on 2018-12-13 04:24

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('about', '0002_comment_username'),
    ]

    operations = [
        migrations.AlterField(
            model_name='comment',
            name='username',
            field=models.TextField(default=None, max_length=15),
        ),
    ]
