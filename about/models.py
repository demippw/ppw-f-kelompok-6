from django.db import models
from datetime import datetime
# Create your models here.

class Comment(models.Model):
    username = models.TextField(default=None,max_length=15)
    comment = models.CharField(max_length=300)
    date = models.DateTimeField(auto_now_add=True, null=True)
    
    def dict(self):
        return{
            "username":self.username,
            "comment":self.comment,
            "date":self.date
        }
