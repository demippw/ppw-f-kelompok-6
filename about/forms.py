from django import forms
from .models import Comment
from datetime import datetime


class Comment_Form(forms.ModelForm):
    class Meta:
        model = Comment
        fields = ['comment']
        widgets = {
            'comment': forms.TextInput(attrs={'class': 'form-control', 'id':'id_comment'})
        }

    def __init__(self, *args, **kwargs):
        super(Comment_Form, self).__init__(*args, **kwargs)
        self.fields['comment'].label = "Apa yang Anda pikirkan?"
