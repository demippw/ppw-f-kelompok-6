from django.test import TestCase
from django.test import TestCase
from django.test import Client
from django.urls import resolve
from django.http import HttpRequest
from django.db import IntegrityError
from django.apps import apps
from about.apps import AboutConfig

from . import views
from .models import Comment
from .forms import Comment_Form


class AboutUnitTest(TestCase):
    def test_about_url_is_exist(self):
        response = Client().get('about/')
        self.assertEqual(response.status_code, 404)

    def test_apps(self):
        self.assertEqual(AboutConfig.name, 'about')
        self.assertEqual(apps.get_app_config('about').name, 'about')

    def test__has_meet_the_team(self):
        request = HttpRequest()
        response = views.about(request)
        html_response = response.content.decode('utf8')
        self.assertIn('Meet The Team', html_response)

    def test__has_testimoni(self):
        request = HttpRequest()
        response = views.about(request)
        html_response = response.content.decode('utf8')
        self.assertIn('Testimoni', html_response)


    def test_About_post_success_and_render_the_result(self):
        test = ''
        response_post = Client().post('/about', {'about': test})
        self.assertEqual(response_post.status_code, 301)

        response = Client().get('/about')
        html_response = response.content.decode('utf8')
        self.assertIn(test, html_response)


    def test_About_post_error_and_render_the_result(self):
        test = 'Anonymous'
        response_post = Client().post('/about', {'about': test})
        self.assertEqual(response_post.status_code, 301)

        response = Client().get('/about')
        html_response = response.content.decode('utf8')
        self.assertNotIn(test, html_response)

    def test_comments_url_is_exist(self):
        response = Client().get('/comment/')
        self.assertEqual(response.status_code, 200)

    def test_nama_template(self):
        response = Client().get('/about/')
        self.assertTemplateUsed(response, 'about.html')

    def test_page_url_about_is_not_exist(self):
        response = Client().get('/about')
        self.assertEqual(response.status_code, 301)
