from django.urls import path, include
from . import views
app_name = 'about'


#path for app
urlpatterns = [
    path('about/', views.about, name='about'),
    # path('result/',views.result,name='result'),
    path('comment/',views.comment,name='comment')
    # path('save_testimoni/',views.save_testimoni,name='save_testimoni'),
]
