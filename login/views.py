from django.shortcuts import render
from django.http import JsonResponse,HttpResponseRedirect
from django.contrib.auth import logout as auth_logout

from .forms import Login_Form
from .models import Login
from signup.models import Signup
from homepage.models import progs

# Create views for Login Page
response={}
def login_post(request):
    form = Login_Form(request.POST)
    html = 'login_post.html'
    return render(request,html,{'form':form})

def save_login(request):
    response['form'] = Login_Form()
    form=Login_Form(request.POST or None)
    if request.method == 'POST' and form.is_valid():
        member_signup = Signup.objects.all()
        for signup in member_signup:
            if signup.email == request.POST['email'] and signup.password == request.POST['password']:
                response['email'] = request.POST['email']
                response['password'] = request.POST['password']
                #Store user into login data
                isMember = Login(email=response['email'],password=response['password'])
                isMember.save()
                status='True'
                return JsonResponse({'status':status})
    status='False'
    return JsonResponse({'status':status})
def logout(request):
    request.session.flush()
    auth_logout(request)
    return HttpResponseRedirect('/home/')
def home(request):
    progss = progs.objects.all()
    return render(request, 'homepage.html', {'progss':progss})