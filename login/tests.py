from django.test import TestCase
from django.test import Client
from django.urls import resolve
from django.http import HttpRequest
from django.db import IntegrityError
import json
from django.apps import apps
from login.apps import LoginConfig

from .views import login_post
from .views import save_login
from .models import Login
from .forms import Login_Form

# Create Unit Test For Login Page

class LoginUnitTest(TestCase):
    def test_login_url_is_exist(self):
        response=Client().get('/login_post/')
        self.assertEqual(response.status_code,200)

    def test_apps(self):
        self.assertEqual(LoginConfig.name, 'login')
        self.assertEqual(apps.get_app_config('login').name, 'login')

    def test_page_using_login_post_function(self):
        found=resolve('/login_post/')
        self.assertEqual(found.func,login_post)

    def test_page_using_save_login_function(self):
        found=resolve('/Save_Login/')
        self.assertEqual(found.func,save_login)

    def test_page_using_login_template(self):
        response=Client().get('/login_post/')
        self.assertTemplateUsed(response,'login_post.html')

    def test_button_responsive(self):
        request=HttpRequest()
        respons = login_post(request)
        html_respons = respons.content.decode('utf8')
        self.assertIn('Login',html_respons)

    def test_login_model_exist(self):
        Login.objects.create(email='laila.saffanah@gmail.com', password='inipassword')
        counting_all_login_objects = Login.objects.all().count()
        self.assertEqual(counting_all_login_objects,1)

    def test_login_form_exist(self):
        form = Login_Form()
        self.assertIn("email",form.as_p())
        self.assertIn("password",form.as_p())

    def test_login_empty_input(self):
        data={'email':''}
        form=Login_Form(data)
        self.assertFalse(form.is_valid())

    def test_form_linked_to_model(self):
        request=HttpRequest()
        request.method="POST"
        request.POST['email'] = "laila.saffanah@gmail.com"
        request.POST['password'] = "inipassword"
        login_post(request)

    def test_login_success_and_render_the_result(self):
        email='saffa@gmail.com'
        password = 'inipassword'
        response_post=Client().post('/Save_Login/',{'email':email,'password':password})
        self.assertEqual(response_post.status_code,200)

    def test_login_from_validation_for_blank_field(self):
        form=Login_Form(data={'email':''})
        self.assertFalse(form.is_valid())
        self.assertEqual(
            form.errors['email'],
            ["This field is required."]
    )
    def test_post_error_and_does_not_exists_in_the_database(self):
        Client().post('/Save_Login/',{'email': ''})
        count_data = Login.objects.all().count()
        self.assertEqual(count_data, 0)

    def test_save_login_url_exist(self):
        response=Client().get('/Save_Login/')
        self.assertEqual(response.status_code,200)

    def test_JSON_object_failed_created(self):
        create_model = Login.objects.create(email='laila@saffa.com',password='inipassword')
        self.assertRaises(IntegrityError)

    def test_challenge10_subscribers_json_exist(self):
        response = self.client.post('/Save_Login/')
        self.assertEqual(response.status_code, 200)
        self.assertJSONEqual(
            str(response.content, encoding='utf8'),
            {'status':'False'}
        )
