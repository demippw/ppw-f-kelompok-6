from django.urls import path
from . import views

urlpatterns=[
    path(r'login_post/',views.login_post,name="login_post"),
    path(r'Save_Login/',views.save_login,name="Save_Login"),
    path(r'logout/',views.logout,name="logout"),
    path(r'home/',views.home,name="home"),
]