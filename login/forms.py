from django import forms

class Login_Form(forms.Form):
    error_messages={
        'required':'Required Field'
    }
    email_attrs={
        'class':'form-control',
        'placeholder':'text@example.com'
    }
    password_attrs={
        'class':'form-control',
        'placeholder':'Your Password'    
    }
    email=forms.EmailField(
        label='Email',
        required=True,
        widget=forms.EmailInput(attrs=email_attrs))

    password=forms.CharField(
        label='Password',
        required=True,
        widget=forms.PasswordInput(attrs=password_attrs))

    