from django.shortcuts import render
from submit_donasi.models import Donmodels
from django.http import JsonResponse
# Create your views here.
response = {'Author': "Hermawan Arifin"}
def history(request):
    return render(request, "history.html")
    
def historian(request):
    if(request.user.is_authenticated):
        return JsonResponse({"result":[obj.dict() for obj in Donmodels.objects.filter(email = request.user.email)]}, content_type = 'application/json')
        