from django.urls import path
from .views import history, historian

urlpatterns = [
    path('history/', history),
    path('daftar/', historian, name="riwayat")
]