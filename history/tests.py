from django.test import TestCase, Client
from django.apps import apps
from history.apps import HistoryConfig
from .views import history
# Create your tests here.

class historyTest(TestCase):
    def test_history_exist(self):
        response = Client().get('/history/')
        self.assertEqual(response.status_code, 200)

    def test_template(self):
        response = Client().get('/history/')
        self.assertTemplateUsed(response, "history.html")

    def test_apps(self):
        self.assertEqual(HistoryConfig.name, 'history')
        self.assertEqual(apps.get_app_config('history').name, 'history')
