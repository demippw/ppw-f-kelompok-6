# **Tugas 2 PPW: Crowd Funding**

Perancangan dan Pemrograman Web| Fakultas Ilmu Komputer, Universitas Indonesia, Semester Ganjil 2018/2019
Kelompok 6

## Nama-nama Anggota Kelompok

1. Hermawan Wibisana Arifin   	(1706043405)
2. Laila Saffanah				(1706043531)
3. Puspacinantya				(1706043821)
4. Salsabila Maurizka			(1706043986)


Tujuan Pembelajaran Khusus:
1. Mengimplementasikan Disiplin ​Test Driven Development dengan menerapkan ​unit test. 
2. Memahami dan Membuat template HTML yang menerapkan ​Styling CSS dan Responsive Web Design 
3. Memahami dan mengimplementasikan penyimpanan data dengan Models (ORM) pada Django, Cookies, dan/atau Local/Session Storage pada ​client 
4. Menggunakan ​library seperti ​OAuth 2.0 dan memanfaatkan​ Webservice. 
5. Memahami dan mengimplementasikan  Javascript, Ajax.  
6. Memahami dan mengimplementasi pemanfaatan Session & Cookies data 

## Status pipelines
![Pipeline status](https://gitlab.com/demippw/ppw-f-kelompok-6/badges/master/build.svg)

## Test Coverage
![Django Coverage](https://gitlab.com/demippw/ppw-f-kelompok-6/badges/master/coverage.svg)

## Coverage Report
[![pipeline status](https://gitlab.com/demippw/ppw-f-kelompok-6/badges/master/pipeline.svg)](https://gitlab.com/demippw/ppw-f-kelompok-6/commits/master)

## Link Herokuapp
https://demippw.herokuapp.com

