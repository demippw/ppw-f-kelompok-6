from django.test import TestCase
from django.test import Client
from django.urls import resolve
from django.apps import apps
from category.apps import CategoryConfig


from .views import category
# Create your tests here.
class CategoryUnitTest(TestCase):
	def test_apps(self):
		self.assertEqual(CategoryConfig.name, 'category')
		self.assertEqual(apps.get_app_config('category').name, 'category')

	def test_category_url_is_exist(self):
		response = Client().get('/category')
		self.assertEqual(response.status_code, 301)

	def test_bencana_alam_url_is_exist(self):
		response = Client().get('/bencana')
		self.assertEqual(response.status_code, 301)

	def test_bantuan_medis_url_is_exist(self):
		response = Client().get('/medis')
		self.assertEqual(response.status_code, 301)
