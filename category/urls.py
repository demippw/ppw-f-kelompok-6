from django.urls import path, include
from . import views
app_name = 'category'


#path for app
urlpatterns = [
    path('category/', views.category, name='category'),
    path('bencana/', views.bencana, name='bencana'),
    path('medis/', views.medis, name='medis'),
]
