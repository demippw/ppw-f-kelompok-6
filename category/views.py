from django.shortcuts import render, redirect
from django.http import HttpResponseRedirect
from .models import Category
from .models import Bencana
from .models import Medis

response={'Author': "Salsabila Maurizka"}
# Create your views here.
def category(request):
	return render(request, 'category.html')

def bencana(request):
    return render(request, 'bencanaalam.html')

def medis(request):
    return render(request, 'bantuanmedis.html')
