from django.db import models

# Create your models here.

class Category(models.Model):
    title = models.CharField(max_length = 30)
    image = models.URLField()
    date = models.DateTimeField()

class Bencana(models.Model):
    title = models.CharField(max_length = 30)
    image = models.URLField()
    date = models.DateTimeField()

class Medis(models.Model):
    title = models.CharField(max_length = 30)
    image = models.URLField()
    date = models.DateTimeField()
