from django.shortcuts import render
from django.http import JsonResponse
from django.db import IntegrityError

from .forms import Signup_Form
from .models import Signup

# Create views for SignUp Page
response={}
def signup_post(request):
    form = Signup_Form(request.POST)
    html = 'signup_post.html'
    return render(request,html,{'form':form})

def save_signup(request):
    response['form']=Signup_Form()
    form=Signup_Form(request.POST or None)
    if request.method == 'POST'and form.is_valid():
        response['fullname']= request.POST['fullname']
        response['username']= request.POST['username']
        response['email']= request.POST['email']
        response['password']= request.POST['password']
        status = 'True'
        #Store models to signup database
        try:
            member=Signup(fullname=response['fullname'],username=response['username'],email=response['email'],password=response['password'])
            member.save()
            return JsonResponse({'status':status})
        except IntegrityError as e:
            status = 'False'
            return JsonResponse({'status':status})
    status = 'False'
    return JsonResponse({'status':status})

       
