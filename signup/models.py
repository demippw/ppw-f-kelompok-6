from django.db import models

class Signup(models.Model):
    fullname = models.CharField(max_length=30)
    username = models.CharField(max_length=15)
    email = models.EmailField(unique=True,db_index=True)
    password=models.CharField(max_length=15)