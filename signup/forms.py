from django import forms

class Signup_Form(forms.Form):
    error_messages={
        'required': 'Required Field'
    }
    fullname_attrs={
        'class':'form-control',
        'placeholder': 'Your Fullname'
    }
    username_attrs={
        'class':'form-control',
        'placeholder': 'Your Username'
    }
    email_attrs={
        'class':'form-control',
        'placeholder': 'text@example.com'
    }
    password_attrs={
        'class':'form-control',
        'placeholder': 'Your Password'
    }
    fullname=forms.CharField(
        label='Fullname',
        required=True,
        max_length=30,
        widget=forms.TextInput(attrs=fullname_attrs))

    username=forms.CharField(
        label='Username',
        required=True,
        max_length=15,
        widget=forms.TextInput(attrs=username_attrs))

    email=forms.EmailField(
        label='Email',
        required=True,
        max_length=30,
        widget=forms.EmailInput(attrs=email_attrs))

    password=forms.CharField(
        label='Password',
        required=True,
        widget=forms.PasswordInput(attrs=password_attrs))
      