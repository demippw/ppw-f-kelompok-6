$(document).ready(function(){
    $('#signup-form').submit(function(e){
        e.preventDefault();
        $.ajax({
            method:'POST',
            url:'/Save_Signup/',
            data:$('form').serialize(),
            success:function(status){
                if(status.status=='True'){
                    $('#buat-alert').html("<div class='alert alert-success' role='alert'>Anda telah terdaftar sebagai member</div>");
                }
                else{
                    $('#buat-alert').html("<div class='alert alert-danger' role='alert'>Anda sudah memiliki akun disini</div>");
                }
            }
        });
    });
    return false
});