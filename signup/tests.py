from django.test import TestCase
from django.test import Client
from django.urls import resolve
from django.http import HttpRequest
from django.db import IntegrityError
from django.apps import apps
from .apps import SignupConfig

from .views import signup_post,save_signup
from .models import Signup
from .forms import Signup_Form


class SignupUnitTest(TestCase):
    def test_UserForm_invalid(self):
        form = Signup_Form(data={'fullname': "", 'username': "mp", 'email': "mp@mp.com", 'password': ""})
        self.assertFalse(form.is_valid())
        
    def test_signup_url_is_exist(self):
        response=Client().get('/signup_post/')
        self.assertEqual(response.status_code,200)

    def test_apps(self):
        self.assertEqual(SignupConfig.name, 'signup')
        self.assertEqual(apps.get_app_config('signup').name, 'signup')

    def test_using_signup_post_function(self):
        found=resolve('/signup_post/')
        self.assertEqual(found.func,signup_post)

    def test_using_save_signup_function(self):
        found=resolve('/Save_Signup/')
        self.assertEqual(found.func,save_signup)

    def test_using_signup_template(self):
        response = self.client.get('/signup_post/')
        self.assertTemplateUsed(response,'signup_post.html')

    def test_signup_model_exist(self):
        Signup.objects.create(fullname='saffa',username='saffa02',email='laila@saffa.com',password='inipassword')
        counting_all_signup_objects = Signup.objects.all().count()
        self.assertEqual(counting_all_signup_objects,1)

    def test_signup_form_exist(self):
        form = Signup_Form()
        self.assertIn("fullname",form.as_p())
        self.assertIn("username",form.as_p())
        self.assertIn("email",form.as_p())
        self.assertIn("password",form.as_p())

    def test_signup_field_empty_input(self):
        data={'fullname':''}
        form=Signup_Form(data)
        self.assertFalse(form.is_valid())

    def test_from_validation_for_blank_field(self):
        form=Signup_Form(data={'email':''})
        self.assertFalse(form.is_valid())
        self.assertEqual(
            form.errors['email'],
            ["This field is required."]
        )

    def test_form_linked_to_model(self):
        request=HttpRequest()
        request.method="POST"
        request.POST['fullname'] = "Laila Saffanah"
        request.POST['username'] = "lailasaffa"
        request.POST['email'] = "laila.saffanah@gmail.com"
        request.POST['password'] = "inipassword"
        signup_post(request)

    def test_JSON_object_failed_created(self):
        create_model = Signup.objects.create(fullname='saffa',username='saffa02',email='laila@saffa.com',password='inipassword')
        self.assertRaises(IntegrityError)

    def test_signup_subscribers_json_exist(self):
        response = self.client.post('/Save_Signup/')
        self.assertEqual(response.status_code, 200)
        self.assertJSONEqual(
            str(response.content, encoding='utf8'),
            {'status': 'False'}
        )
