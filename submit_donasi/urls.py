from django.urls import path
from . import views

app_name='submit_donasi'

urlpatterns=[
    # path('thankyou/', thankyou, name='thankyou'),
    path('submitdon/',views.showdonation, name='submitdon'),
    path('Add_Donation/',views.submitdon, name='Add_Donation'),
    path('thankyou/', views.showdonatur, name='thankyou'),
    # path('filledsub/', views.filledsub, name='filledsub'),
]