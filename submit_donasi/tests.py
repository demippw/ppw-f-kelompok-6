from django.test import TestCase, RequestFactory
from django.test import Client
from django.urls import resolve
from django.http import HttpRequest
from .models import Donmodels
from .forms import donforms
from signup.models import Signup
from django.contrib.auth.models import AnonymousUser, User
from django.db import IntegrityError
from django.apps import apps
from submit_donasi.apps import SubmitDonasiConfig
from .views import showdonation, showdonatur, submitdon

class SubmitDonasiConfigTest(TestCase):
    def test_apps(self):
        self.assertEqual(SubmitDonasiConfig.name, 'submit_donasi')
        self.assertEqual(apps.get_app_config('submit_donasi').name, 'submit_donasi')


class SubmitDonasiUnitTest(TestCase):
    def setUp(self):
        self.factory = RequestFactory()

    def test_page_url_submitdon_is_exist(self):
        response = Client().get('/submitdon/')
        self.assertEqual(response.status_code, 200)

    def test_page_url_addDon_is_not_exist(self):
        response = Client().get('/Add_Donation/')
        self.assertEqual(response.status_code, 302)


    # def test_page_url_filledsub_is_exist(self):
    #     response = Client().get('/filledsub/')
    #     self.assertEqual(response.status_code, 200)

    # def test_using_showdon_function(self):
    #     found = resolve('/submitdon/')
    #     self.assertEqual(found.func,showdonation)
    #
    # # def test_using_filledsub_function(self):
    # #     found = resolve('/filledsub/')
    # #     self.assertEqual(found.func,filledsub)
    #
    # def test_using_submitdon_function(self):
    #     found = resolve('/Add_Donation/')
    #     self.assertEqual(found.func,submitdon)

    # def test_using_templates_in_views(self):
    #     response = Client().get('/submitdon/')
    #     response2 = Client().get('/thankyou/')
    #     self.assertTemplateUsed(response, 'submit_donasi.html')
    #     self.assertTemplateUsed(response2, 'thankyou.html')


    def test_model_can_create_new_donation(self):
        self.client.get('/submitdon/')
        Donmodels.objects.create(name='user1', email='test@test.com', title='Bantu Korban Gempa dan Tsunami Palu', nominal ='15000000',  publishedname='Donasi sebagai anonim')

        counting_all_donation = Donmodels.objects.all().count()
        self.assertEqual(counting_all_donation, 1)

    # def test_model_cannot_create_new_donation_if_not_authenticated(self):
    #     Donmodels.objects.create(name='null', email= 'null', title='Bantu Korban Gempa dan Tsunami Palu', nominal ='787878',  publishedname='Donasi sebagai anonim')

    #     counting_all_donation = Donmodels.objects.all().count()
    #     self.assertEqual(counting_all_donation, 0)

    def test_met_integrity_error(self):
        self.client.login(username="user1", email='test@test.com', password="passwd1")
        Donmodels.objects.create(name='user1', email='test@test.com', title='Bantu Korban Gempa dan Tsunami Palu', nominal ='15000000',  publishedname='Donasi sebagai anonim')
        self.assertRaises(IntegrityError)


    def test_form_validation_for_blank_field(self):
        form= donforms(data={'nominal': ''})
        self.assertFalse(form.is_valid())
        self.assertEqual(
            form.errors['nominal'],
            ["This field is required."]
        )

    # def test_form_is_shown(self):
    #     form= donforms()
    #     self.assertIn('class=form-control', form.as_p)

    # def test_cannot_submit_if_not_authenticated(self):
    #     response = Client().get('/submitdon/')
    #     Donmodels.objects.create(name='null', email= 'null', title='Bantu Korban Gempa dan Tsunami Palu', nominal ='787878',  publishedname='Donasi sebagai anonim')
    #     self.assertRedirects(response, '/login_post/')



    def test_if_can_submit_302(self):
        dummy_user = User.objects.create_user(username='dummy', email='dummy@gmail.com', password='top_secret')
        data = {'name': 'username', 'email':'dummy@gmail.com', 'title':'Bantu Korban Gempa dan Tsunami Palu','nominal': 100000, 'publishedname': 'yes'}
        request = self.factory.post('/Add_Donation/',data)

        request.session = self.client.session
        request.session['username'] = 'dummy'
        request.session['email'] = 'dummy@gmail.com'
        request.session.save()

        request.user = dummy_user
        response = submitdon(request)
        self.assertEqual(response.status_code, 302)


    # def test_submit_donation_post_success(self):
    #     Signup.objects.create(fullname="bisakok", username='ppwbisa', email="bisakok@gmail.com", password="ppwppw")
    #     jud = 'Bantu Korban Gempa dan Tsunami Palu'
    #     nam = 'puspa'
    #     em = 'bisakok@gmail.com'
    #     nom = '15000000'
    #     pub = 'Donasi sebagai anonim'
    #     response_post = Client().post('/Add_Donation/', {'title': jud, 'nama': nam, 'email': em, 'nominal': nom, 'publishedname': pub})
    #     self.assertEqual(response_post.status_code, 200)

        # response= Client().get('/riwayat/')
        # html_response = response.content.decode('utf8')
        # self.assertIn(nom, jud, html_response)

    # def test_submit_donation_post_fail(self):
    #     Signup.objects.create(fullname="bisakok", username='ppwbisa', email="haduh@gmail.com", password="ppwppw")
    #     jud = 'Bantu Korban Gempa dan Tsunami Palu'
    #     nam = 'puspa'
    #     em = 'gilagila@gmail.com'
    #     nom = '15000000'
    #     pub = 'Donasi sebagai anonim'
    #     response_post = Client().post('/Add_Donation/', {'title': jud, 'nama': nam, 'email': em, 'nominal': nom, 'publishedname': pub})
    #     self.assertEqual(response_post.status_code, 302)
