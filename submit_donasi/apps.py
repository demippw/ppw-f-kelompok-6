from django.apps import AppConfig


class SubmitDonasiConfig(AppConfig):
    name = 'submit_donasi'
