#from django.core.validators import validate_email
from django.db import models

# Create your models here.

class Donmodels (models.Model):
    title = models.TextField()
    name = models.TextField(max_length=10, default=None)
    email = models.EmailField(max_length=254, default='test@example.com',db_index=True)
    nominal = models.IntegerField()
    publishedname = models.TextField(max_length=3, default=None)

    def dict(self):
        return{
            "title":self.title,
            "nominal":self.nominal,
            "email":self.email
        }

