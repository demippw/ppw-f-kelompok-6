from django import forms    
from .models import Donmodels

class donforms(forms.Form):
    doncategory = [
        ('korban gempa dan tsunami Palu','Bantu Korban Gempa dan Tsunami Palu'),
        ('korban gempa Lombok', 'Bantu Korban Gempa Lombok'),
        ('korban di Donggala','Bantu Korban Donggala'),
        ('Pak Amin','Bantu Pak Amin Lawan Tumor di Mulutnya'),
        ('Eza','Bantu Eza Agar dapat Melihat Kembali'),
        ('Rano','Bantu Rano dari Kakinya yang Putus'),
        ('1000 Tenda untuk Pengungsi Lombok','1000 Tenda untuk Pengungsi Lombok'),
        ('anak-anak Korban Tsunami Palu', 'Bantu Anak-anak Korban Tsunami Palu'),
        ('korban Bencana SulTeng', 'Untuk Korban Bencana SulTeng'),
        ('Jamilah', 'Bantu Sembuhkan Kanker Tiroid Jamilah'),
        ('operasi katarak gratis', 'Bantuan Operasi Katarak Gratis'),
        ('RS Apung untuk Timur Indonesia', 'RS Apung untuk Timur Indonesia'),
    ]

    donkepentingan =[
        ('yes','Donasi sebagai anonim'),
        ('no','Tulis nama saya'),
    ]

    # category = forms.CharField(label='Kategori', required=True, widget=forms.Select(choices=doncategory, attrs={'class' : 'form-control','placeholder': 'Pilih Kategori..', 'required': 'Harap diisi'}))
    title = forms.CharField(
        label='Nama Program', 
        required=True, 
        widget=forms.Select(choices=doncategory, 
        attrs={'class' : 'form-control','placeholder': 'Pilih Kategori..', 'required': 'Harap diisi'}))
    # nama = forms.CharField(
    #     label= 'Nama Lengkap', 
    #     required=True,  
    #     widget=forms.TextInput(attrs={'class' : 'form-control','placeholder': 'Tulis Nama..', 'required': 'Harap diisi'}))
    # email = forms.EmailField(
    #     label= 'Alamat Email', 
    #     required=True, 
    #     widget=forms.EmailInput({'class': 'form-control', 'placeholder': 'test@example.com', 'required': 'Harap diisi'}))
    nominal = forms.IntegerField(
        label='Nominal Donasi', 
        required=True,  
        widget=forms.NumberInput(attrs={'class' : 'form-control','placeholder': 'Tulis Nominal..', 'required': 'Harap diisi'}))
    publishedname = forms.ChoiceField(
        label='Apakah Anda ingin donasi anda sebagai anonim?', 
        required=True,
        choices=donkepentingan,
        widget=forms.RadioSelect())


