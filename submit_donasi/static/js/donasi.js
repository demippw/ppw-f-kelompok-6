$(document).ready(function(){
    $("#form").submit(function(e) {
        $.ajax({
            type: "POST",
            url: "/filledsub/",
            data: $('form').serialize(),
            success: function(results) {
                var toPrint = ''
                if(results.stat == 'invalid') {
                    toPrint += '<div class="alert alert-danger" role="alert" style="text-align: center">The button is missing! Use another email and then it will be back</div>'
                    $('#alertField').html(toPrint)
                }
                else{
                    window.location.replace("/thankyou/");
                }                
            }        
        });
        e.preventDefault();
    });

    // $.ajax ({
    //     type: "GET",
    //     url: "/showriwayat/",
    //     dataType: "json",
    //     success: function(result) {
    //         var riwayatt = "";
    //         for (var i = 0; i < result.length; i++) {
    //             var jud = result[i].titlesubmitted;
    //             var nom = result[i].nominalsubmitted;
    //             riwayatt += "<tr><td>" + jud + "</td>"
    //             riwayatt += "<td>" + nom + "</td></tr>"
    //         }
    //         $("#tableBody").html(riwayatt); 
    //     }
    // });
});