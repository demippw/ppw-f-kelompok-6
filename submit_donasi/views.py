from django.shortcuts import render
from django.http import HttpResponseRedirect
from django.http import JsonResponse
# from django.utils.datastructures import MultiValueDictKeyError
# from django.contrib import messages
from .models import Donmodels
from .forms import donforms
from signup.models import Signup
from django.db import IntegrityError
from google.auth.transport import requests as requests2
from google.oauth2 import id_token


# Create your views here.

response = {}
def showdonation(request):
    form = donforms(request.POST)
    html = 'submit_donasi.html'
    return render(request,html,{'form':form})

# def submitdon(request):
#     # if request.user.is_authenticated:
#     #     request.session['username'] = request.user.username
#     #     request.session['email'] = request.user.email

#         response['form'] = donforms()
#         form = donforms(request.POST or None)
#         if(request.method == 'POST' and form.is_valid()):
#             email_utk_dicek = request.POST['email']
#             hasil = Signup.objects.filter(email = email_utk_dicek)
#             if(hasil != None):
#             # registered = Signup.objects.all()
#             # for member in registered:
#             #     if(member.email  request.POST['email']):
#                 response['title'] = request.POST['title']
#                 response['nama'] = request.POST['nama']
#                 response['email'] = request.POST['email']
#                 response['nominal'] = request.POST['nominal']
#                 response['publishedname'] = request.POST['publishedname']
#                 writedon = Donmodels(title=response['title'],nama=response['nama'], email=response['email'], nominal=response['nominal'], publishedname=response['publishedname'])
#                 writedon.save()
#                 response['status'] = 'valid'
#                 # list_donatur = Donmodels.objects.all()
#                 # response['list_donatur']=list_donatur
#                 # html = 'riwayat.html'
#                 # return render(request,html,response)
#                 # return HttpResponseRedirect('/riwayat/')
#             else:
#                 response['status'] = 'invalid'
#                 return render(request, 'submit_donasi.html', response)
#         html = 'riwayat.html'
#         return render(request,html,response)

def submitdon(request):
    if request.user.is_authenticated:
        request.session['username'] = request.user.username
        request.session['email'] = request.user.email
        # status = 'valid'

        response['form'] = donforms()
        if(request.method == 'POST'):
            response['name'] = request.session['username']
            response['email'] = request.session['email']
            response['title'] = request.POST['title']
            response['nominal'] = request.POST['nominal']
            response['publishedname'] = request.POST['publishedname']
            # try:
            writedon = Donmodels(name=response['name'], email= response['email'], title=response['title'], nominal=response['nominal'], publishedname=response['publishedname'])
            writedon.save()
            return HttpResponseRedirect('/thankyou/')
                # return JsonResponse({'stat': status})
            # except IntegrityError as e:
            #     # status = 'invalid'
            #     return HttpResponseRedirect('/login_post/')
            #     # return JsonResponse({'stat': status})
    else:
        # status = 'invalid'
        return HttpResponseRedirect('/login_post/')
        # return JsonResponse({'stat': status})
    
# def filledsub(request):
#     all_obj = Donmodels.objects.all().values()
#     filled = list(all_obj)
#     return JsonResponse(filled, safe= False)

def showdonatur(request):
    list_donatur = Donmodels.objects.all()
    response['list_donatur']=list_donatur
    return render(request, 'thankyou.html',response)

# def riwayat(request):
#     return render(request, 'riwayat.html', response)

# def showriwayat(request):
#     total = 0
#     email = request.session['email']
#     submitted = Donmodels.objects.filter(email=email)
#     arr = []
#     for i in submitted:
#         data = {}
#         data['titlesubmitted'] = i.title
#         data['nominalsubmitted'] = i.nominal
#         arr.append(data)
#         total += 'nominalsubmitted'
#     return JsonResponse(arr)
