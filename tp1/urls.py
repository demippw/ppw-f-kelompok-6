"""tp1 URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.contrib.auth import views
from django.conf.urls import url,include
from homepage import urls as home
from category import urls as category
from about import urls as about
from login import urls as login_page
from signup import urls as signup_page
from history import urls as history
from submit_donasi import urls as submit_donasi

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url('auth/', include('social_django.urls', namespace='social')),
    url('', include(home)),
	url('', include(category)),
    url('', include(about)),
    url('', include(login_page)),
    url('', include(history)),
    url('', include(signup_page)),
    url('',include(submit_donasi)),
]
