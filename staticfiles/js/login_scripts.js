$(document).ready(function(){
    $('#login-form').submit(function(e){
        e.preventDefault();
        $.ajax({
            method:'POST',
            url:'/Save_Login/',
            data:$('#login-form').serialize(),
            success:function(status){
                if(status.status=='True'){
                    swal("Selamat!", "Anda sudah masuk", "success");
                }
                else{
                    swal("Oops!", "Anda belum memiliki akun disini", "error");
                }
            }
        });
        return false;
    });
    
});

// function onSignIn(googleUser){
//     var profile = googleUser.getBasicProfile();
//     var id_token = googleUser.getAuthResponse().id_token;

//     function signIn(token){
//         var csrftoken = $("[name-csrfmiddlewaretoken]").val();
//         $.ajax({
//             method:"POST",
//             url: "/login_post/",
//             headers:{
//                 "X-CSRFToken": csrftoken
//             },
//             data:{id_token: token},
//             success: function(result){
//                 console.log("Sign in success");
//                 if(result.status === "0"){
//                     window.location.replace(result.url);
//                 }
//                 else{
//                     html = "<h3>Sorry! Something went wrong</h3>"
//                 }
//             },
//             error: function(error){
//                 alert("Sorry! Error has occured")
//             }
//         })

//     }
// }