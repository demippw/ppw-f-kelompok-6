from django.test import TestCase, Client
from django.urls import resolve
from django.apps import apps
from homepage.apps import HomepageConfig
from .views import home
from .models import progs
# Create your tests here.
class HomeUnitTest(TestCase):
    def test_home_is_exist(self):
        response=Client().get('/home/')
        self.assertEqual(response.status_code, 200)

    def test_data_base(self):
        terserah = progs.objects.create()
        self.assertEqual(progs.objects.count(),1)

    def test_apps(self):
        self.assertEqual(HomepageConfig.name, 'homepage')
        self.assertEqual(apps.get_app_config('homepage').name, 'homepage')
